# Projet personnel en JavaScript

- Cette application reconnait les émotions d’une personne à travers une image caméra     

# Prérequis

- Installer Visual Studio Code avec le plugin Live Server
- Cloner le projet qui se trouve sur gitlab sur votre ordinateur


# Démarrage

- Ouvrir le projet avec Visual Studio Code et lancer le fichier index.html avec Live Server 
- L'application démarre en locale sur votre navigateur  par défaut 
- Donnée l'autorisation à votre navigateur pour l'utilisation de la webcam  

# Conclusion 

- Les émotions doivent être affiché en même temps que votre image sur le navigateur  
